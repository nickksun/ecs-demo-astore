#!/usr/bin/env python3

import boto3
from botocore.exceptions import ClientError

ecs_cluster = "astore"
desired_tasks = 2

ecs_client = boto3.client('ecs')

asg_client = boto3.client('autoscaling')
paginator = asg_client.get_paginator('describe_auto_scaling_groups')

if __name__ == '__main__':
    ecs_services = ecs_client.list_services(
        cluster=ecs_cluster
    )
    ecs_services = ecs_client.describe_services(
        cluster=ecs_cluster,
        services=ecs_services['serviceArns']
    )

    # Scaling-in services
    if 'services' in ecs_services:
        for ecs_service in ecs_services['services']:
            if 'serviceName' in ecs_service:
                service_name = ecs_service['serviceName']
                prev_desired_count = ecs_service['desiredCount']
                try:
                    ecs_client.update_service(
                        cluster=ecs_cluster,
                        service=service_name,
                        desiredCount=desired_tasks
                    )
                    ecs_tasks = ecs_client.list_tasks(
                        cluster=ecs_cluster
                    )
                    for ecs_task in ecs_tasks["taskArns"]:
                        ecs_client.stop_task(
                            cluster=ecs_cluster,
                            task=ecs_task
                        )
                    print("updated service: {} from {} to {}".format(service_name, prev_desired_count, desired_tasks))
                except ClientError as err:
                    print (err)

    # Scaling-in cluster
    page_iterator = paginator.paginate(
        PaginationConfig={'PageSize': 100}
    )
    try:
        filtered_asgs = page_iterator.search(
            'AutoScalingGroups[] | [?contains(Tags[?Key==`{}`].Value, `{}`)]'.format(
                'ecs-cluster', ecs_cluster)
        )

        for asg in filtered_asgs:
            # if (need_scale_in and asg['MinSize'] < asg['DesiredCapacity']):
            asg_client.set_desired_capacity(
                AutoScalingGroupName=asg['AutoScalingGroupName'],
                DesiredCapacity=asg['MinSize'],
                HonorCooldown=False
            )
            print("updated ASG: {} from {} to {}".format(
                asg['AutoScalingGroupName'],
                asg['DesiredCapacity'],
                asg['MinSize']
            ))

    except ClientError as err:
        print (err)
