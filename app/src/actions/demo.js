export const GRAPH_DATA_REQUEST = 'GRAPH_DATA_REQUEST';
export const GRAPH_DATA_RECEIVE = 'GRAPH_DATA_RECEIVE';
export const REQUEST_SENT = 'REQUEST_SENT';
export const RESPONSE_RECEIVED = 'RESPONSE_RECEIVED';

function getGraphDataRequest() {
    return {
        type: GRAPH_DATA_REQUEST
    }
}

function getGraphDataReceive(service, data) {
    return {
        type: GRAPH_DATA_RECEIVE,
        service,
        data
    }
}

export function getGraphData(service) {
    return dispatch => {
        dispatch(getGraphDataRequest());
        return fetch('https://ecs-demo-putoutfire.nickksun.me/task-amount?service=' + service, {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }
        ).then(
            function (response) {
                return response.json();
            }
        ).then(function (jsonData) {
            dispatch(getGraphDataReceive(service, jsonData));
        });
    }
}

function requestSent() {
    return {
        type: REQUEST_SENT
    }
}

function responseReceived(service, data) {
    return {
        type: RESPONSE_RECEIVED,
        service,
        data
    }
}

export function sendRequest(service) {
    return dispatch => {
        requestSent();
        var options = {
            method: 'get',
            mode: 'no-cors'
        };
        if (service === 'fire') {
            options = {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            };
        }
        return fetch(
            'http://demo-putoutfire-906181697.ap-southeast-2.elb.amazonaws.com/' + service,
            options
        ).then(function (response) {
            if(service !== 'fire') {
                dispatch(getGraphData('extinguisher'));
                return;
            }
            return response.json();
        }).then(function (jsonData) {
            dispatch(responseReceived(service, jsonData));
        });
    }
}