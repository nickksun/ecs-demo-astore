import React, {Component} from 'react';
import {connect} from 'react-redux';
import {routerActions} from 'react-router-redux';

// import Header from '../components/Header';

import Top from '../components/Header/Top';
import Menu from '../components/Header/Menu';
import Slider from '../components/Header/Slider';
import Footer from '../components/Footer';


class Home extends Component {
    constructor(args){
        super(args);
        this.handleCategoryClick = this.handleCategoryClick.bind(this);
    }


    handleCatalogProduct(productId) {
        const {dispatch} = this.props;
        dispatch(routerActions.replace('/catalog/product/'+productId));
    }

    handleCategoryClick(categoryId) {
        const {dispatch} = this.props;
        dispatch(routerActions.replace('/catalog/category/'+categoryId));
    }

    render() {
        const {dispatch} = this.props;
        return (
            <div>

                <Top/>
                <Menu onCategoryClick={this.handleCategoryClick} />
                <Slider/>
                <div className="wrap">
                    <div id="owl-demo" className="owl-carousel">
                        <div className="item" onClick="location.href='details.html';">
                            <div className="cau_left">
                                <img className="lazyOwl" data-src="assets/astore/images/c1.jpg" alt="Lazy Owl Image"/>
                            </div>
                            <div className="cau_left">
                                <h4><a href="details.html">branded shoes</a></h4>
                                <a href="details.html" className="btn">shop</a>
                            </div>
                        </div>
                        <div className="item" onClick="location.href='details.html';">
                            <div className="cau_left">
                                <img className="lazyOwl" data-src="assets/astore/images/c2.jpg" alt="Lazy Owl Image"/>
                            </div>
                            <div className="cau_left">
                                <h4><a href="details.html">branded tees</a></h4>
                                <a href="details.html" className="btn">shop</a>
                            </div>
                        </div>
                        <div className="item" onClick="location.href='details.html';">
                            <div className="cau_left">
                                <img className="lazyOwl" data-src="assets/astore/images/c3.jpg" alt="Lazy Owl Image"/>
                            </div>
                            <div className="cau_left">
                                <h4><a href="details.html">branded jeens</a></h4>
                                <a href="details.html" className="btn">shop</a>
                            </div>
                        </div>
                        <div className="item" onClick="location.href='details.html';">
                            <div className="cau_left">
                                <img className="lazyOwl" data-src="assets/astore/images/c2.jpg" alt="Lazy Owl Image"/>
                            </div>
                            <div className="cau_left">
                                <h4><a href="details.html">branded tees</a></h4>
                                <a href="details.html" className="btn">shop</a>
                            </div>
                        </div>
                        <div className="item" onClick="location.href='details.html';">
                            <div className="cau_left">
                                <img className="lazyOwl" data-src="assets/astore/images/c1.jpg" alt="Lazy Owl Image"/>
                            </div>
                            <div className="cau_left">
                                <h4><a href="details.html">branded shoes</a></h4>
                                <a href="details.html" className="btn">shop</a>
                            </div>
                        </div>
                        <div className="item" onClick="location.href='details.html';">
                            <div className="cau_left">
                                <img className="lazyOwl" data-src="assets/astore/images/c2.jpg" alt="Lazy Owl Image"/>
                            </div>
                            <div className="cau_left">
                                <h4><a href="details.html">branded tees</a></h4>
                                <a href="details.html" className="btn">shop</a>
                            </div>
                        </div>
                        <div className="item" onClick="location.href='details.html';">
                            <div className="cau_left">
                                <img className="lazyOwl" data-src="assets/astore/images/c3.jpg" alt="Lazy Owl Image"/>
                            </div>
                            <div className="cau_left">
                                <h4><a href="details.html">branded jeens</a></h4>
                                <a href="details.html" className="btn">shop</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="main_bg1">
                    <div className="wrap">
                        <div className="main1">
                            <h2>featured products</h2>
                        </div>
                    </div>
                </div>

                <div className="main_bg">
                    <div className="wrap">
                        <div className="main">

                            <div className="grids_of_3">
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/pic1.jpg" alt=""/>
                                        <h3>branded shoes</h3>
                                        <div className="price">
                                            <h4>$300<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/pic2.jpg" alt=""/>
                                        <h3>branded t-shirts</h3>
                                        <div className="price">
                                            <h4>$300<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/pic3.jpg" alt=""/>
                                        <h3>branded tees</h3>
                                        <div className="price">
                                            <h4>$300<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="clear"></div>
                            </div>
                            <div className="grids_of_3">
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/pic4.jpg" alt=""/>
                                        <h3>branded bags</h3>
                                        <div className="price">
                                            <h4>$300<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/pic5.jpg" alt=""/>
                                        <h3>ems women bag</h3>
                                        <div className="price">
                                            <h4>$300<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/pic6.jpg" alt=""/>
                                        <h3>branded cargos</h3>
                                        <div className="price">
                                            <h4>$300<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="clear"></div>
                            </div>

                        </div>
                    </div>
                </div>

                <Footer/>

            </div>
        )
    }
}

function mapStateToProps(state) {
    // const {graph} = state;
    // const {service, data} = graph;

    return {
        // service,
        // data
    }
}

export default connect(mapStateToProps)(Home);