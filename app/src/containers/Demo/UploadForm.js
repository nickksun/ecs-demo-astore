import React, {Component} from 'react'
import {connect} from 'react-redux';
import {routerActions} from 'react-router-redux';
import Navibar from '../../components/Navibar';
import NavLink from '../../components/NavLink';


import {uploadPhoto, checkExecutionStatus} from '../../actions/demo';

class UploadForm extends Component {
    constructor(props) {
        super(props);
        this.interval = null;
    }

    componentWillMount() {

    }

    componentWillReceiveProps(nextProps) {
        const {isAuthenticated, dispatch, executionArn, imageID} = nextProps;
        const {isAuthenticated: wasAuthenticated} = this.props;
        if (wasAuthenticated && !isAuthenticated) {
            dispatch(routerActions.replace('/login'));
        }


        const {isPostProcessing} = nextProps;
        const {isPostProcessing: wasPostProcessing} = this.props;

        if (!wasPostProcessing && isPostProcessing && executionArn && imageID) {
            this.interval = setInterval(() => {
                dispatch(checkExecutionStatus(executionArn, imageID))
            }, 1000);
        } else if (wasPostProcessing && !isPostProcessing) {
            this.isPostProcessed = true;
            this.refs.photoupload.value = null;
            clearInterval(this.interval);
        }
    }


    // handleFileChange(e) {
    //     const {dispatch} = this.props;
    //     e.preventDefault();
    //
    // }

    handleUpload(e) {
        this.isPostProcessed = false;
        const {dispatch} = this.props;
        let fileBody = this.refs.photoupload.files[0];
        dispatch(uploadPhoto(fileBody));
    }

    render() {
        const {errorMessage, isUploaded, filename, isFetching, tags} = this.props;
        return (
            <div id="wrapper">
                <Navibar {...this.props}
                        showSubmenu={true}
                />
                <div id="page-wrapper">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-12">
                                <h1 className="page-header">Upload Image</h1>
                                <div className="table-responsive">
                                    {errorMessage &&
                                    <div className="alert alert-danger">{errorMessage}</div>
                                    }

                                    <div className="form-group">
                                        <label htmlFor="photoupload">Select an image
                                            <input ref="photoupload" type="file" accept="image/*"/>
                                        </label>
                                        {!isFetching &&
                                        <button onClick={(e) => this.handleUpload(e)}
                                                className="btn btn-primary">
                                            Upload
                                        </button>
                                        }
                                    </div>
                                </div>

                                {isFetching &&
                                <div>Processing... <i className="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                    <span className="sr-only">Loading...</span>
                                </div>
                                }

                                {isUploaded &&
                                <div className="alert alert-success">Uploaded: {filename}</div>
                                }

                                {this.isPostProcessed &&
                                <div>
                                    <div className="alert alert-success">Processed: {filename}</div>
                                    {tags &&
                                    <div className="alert alert-success">Found tags: <strong>{tags.join(', ')}</strong>
                                    </div>
                                    }

                                </div>
                                }
                                {!isFetching &&
                                <NavLink to="/graph" className="btn btn-primary">Check Graph</NavLink>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const {upload} = state;
    const {isFetching, isAuthenticated, errorMessage, filename, isUploaded, isPostProcessing, executionArn, executionStatus, tags, imageID} = upload;

    return {
        isFetching,
        isAuthenticated,
        filename,
        isUploaded,
        isPostProcessing,
        executionArn,
        executionStatus,
        tags,
        imageID,
        errorMessage
    }
}

export default connect(mapStateToProps)(UploadForm);