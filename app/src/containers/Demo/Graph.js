import React, {Component} from 'react'
import {connect} from 'react-redux';
import {routerActions} from 'react-router-redux';
import Navibar from '../../components/Navibar';
import NavLink from '../../components/NavLink';

import {getGraphData} from '../../actions/demo';

import {HorizontalBar} from 'react-chartjs-2';


class Graph extends Component {
    constructor(props) {
        super(props);
        this.interval = null;
        this.reDrawGraph = false;
        this.graphHeight = 50;
    }

    componentWillMount() {
        const {dispatch, tags} = this.props;
        if (tags.length > 0) {
            this.graphHeight = tags.length * 25;
        }

        this.interval = setInterval(() => {
            dispatch(getGraphData());
            this.reDrawGraph = false;
        }, 2000);
    }

    componentWillReceiveProps(nextProps) {
        let newGraphHeight = nextProps.tags.length * 25;
        if (newGraphHeight !== this.graphHeight) {
            this.graphHeight = newGraphHeight;
            this.reDrawGraph = true;
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        const {tags, values} = this.props;

        let data = {
            labels: tags,
            responsive: true,
            // maintainAspectRatio: false,
            datasets: [
                {
                    label: "image(s)",
                    // backgroundColor: [
                    //     'rgba(255, 99, 132, 0.2)',
                    //     'rgba(54, 162, 235, 0.2)',
                    //     'rgba(255, 206, 86, 0.2)',
                    //     'rgba(75, 192, 192, 0.2)',
                    //     'rgba(153, 102, 255, 0.2)',
                    //     'rgba(255, 159, 64, 0.2)'
                    // ],
                    // borderColor: [
                    //     'rgba(255,99,132,1)',
                    //     'rgba(54, 162, 235, 1)',
                    //     'rgba(255, 206, 86, 1)',
                    //     'rgba(75, 192, 192, 1)',
                    //     'rgba(153, 102, 255, 1)',
                    //     'rgba(255, 159, 64, 1)'
                    // ],
                    borderWidth: 1,
                    data: values
                }
            ]
        };
        let options = {
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {}
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        // stepSize:
                        // max: Math.max(values) + 5
                    }
                }]
            }
        };
        return (

            <div id="wrapper">
                <Navibar {...this.props}
                         showSubmenu={true}
                />
                <div id="page-wrapper">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-12">
                                <h1 className="page-header">Graph <NavLink to="/upload" className="btn btn-primary">Upload
                                    Image</NavLink></h1>
                                {!(tags.length > 0) &&
                                <div>Loading... <i className="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                    <span className="sr-only">Loading...</span>
                                </div>
                                }
                                {(tags.length > 0) &&
                                <div>
                                    <p>Number of Images per Rekognised Attribute</p>
                                    <HorizontalBar
                                        redraw={this.reDrawGraph}
                                        data={data}
                                        options={options}
                                        height={this.graphHeight}
                                    />
                                    <hr/>
                                    <ul className="list-group col-sm-5">
                                        {tags.map((tag, i) => {
                                            return (
                                                <li className="list-group-item">
                                                    <span className="badge">{values[i]}</span>
                                                    {tag}
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </div>
                                }

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

function mapStateToProps(state) {
    const {graph} = state;
    const {isAuthenticated, errorMessage, user, tags, values} = graph;

    return {
        isAuthenticated,
        errorMessage,
        user,
        tags,
        values
    }
}

export default connect(mapStateToProps)(Graph);
