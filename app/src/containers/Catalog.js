import React, {Component} from 'react';
import {connect} from 'react-redux';

import Top from '../components/Header/Top';
import Menu from '../components/Header/Menu';
import Slider from '../components/Header/Slider';
import Footer from '../components/Footer';

class Catalog extends Component {
    render() {
        const {dispatch} = this.props;
        return (
            <div>

                <Top/>
                <Menu onCategoryClick={this.handleCategoryClick} />
                <Slider/>
                <div className="main_bg">
                    <div className="wrap">
                        <div className="main">
                            <h2 className="style top">featured handbags</h2>
                            <div className="grids_of_3">
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/w_pic1.jpg" alt=""/>
                                        <h3>branded handbags</h3>
                                        <div className="price">
                                            <h4>$299<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/w_pic2.jpg" alt=""/>
                                        <h3>branded handbags</h3>
                                        <div className="price">
                                            <h4>$299 <span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/w_pic3.jpg" alt=""/>
                                        <h3>branded handbags</h3>
                                        <div className="price">
                                            <h4>$299<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="clear"></div>
                            </div>
                            <div className="grids_of_3">
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/w_pic4.jpg" alt=""/>
                                        <h3>branded handbags</h3>
                                        <div className="price">
                                            <h4>$299<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/w_pic5.jpg" alt=""/>
                                        <h3>branded handbags</h3>
                                        <div className="price">
                                            <h4>$299<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="grid1_of_3">
                                    <a href="details.html">
                                        <img src="assets/astore/images/w_pic6.jpg" alt=""/>
                                        <h3>branded handbags</h3>
                                        <div className="price">
                                            <h4>$299<span>indulge</span></h4>
                                        </div>
                                        <span className="b_btm"></span>
                                    </a>
                                </div>
                                <div className="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>


        )
    }
}

function mapStateToProps(state) {
    // const {catalog} = state;
    // const {service, data} = catalog;

    return {
        // service,
        // data
    }
}

export default connect(mapStateToProps)(Catalog);