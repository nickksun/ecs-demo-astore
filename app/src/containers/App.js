import React, {Component} from 'react';
import {connect} from 'react-redux';

import Demo from '../components/Demo';

class App extends Component {
    render() {
        const {dispatch} = this.props;
        return (

            <div className="App">
                <div className="App-header">
                    <img src="assets/img/aws-docker.png" className="App-logo" alt="logo"/>
                    <h2>ECS Demo - Put Out Fire</h2>
                    <Demo {...this.props}/>
                </div>
            </div>

        )
    }
}

function mapStateToProps(state) {
    const {graph} = state;
    const {service, data} = graph;

    return {
        service,
        data
    }
}

export default connect(mapStateToProps)(App);