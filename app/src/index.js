import React from 'react';
import {render} from 'react-dom';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import reducer from './reducers';
import {Router, Route, hashHistory} from 'react-router';
import {syncHistoryWithStore, routerMiddleware} from 'react-router-redux';

import Home from './containers/Home';
import Catalog from './containers/Catalog';

require('dotenv').config(); // double checked and this is the correct path.

const middleware = [thunk];
if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger())
}
const baseHistory = hashHistory;
const routingMiddleware = routerMiddleware(baseHistory);
middleware.push(routingMiddleware);
const store = createStore(
    reducer,
    applyMiddleware(...middleware)
);

const history = syncHistoryWithStore(baseHistory, store);

render(
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={Home}/>
            <Route path="/catalog" component={Catalog}/>
        </Router>
    </Provider>
    ,
    document.getElementById('root')
);
