import {
    GRAPH_DATA_RECEIVE,
    RESPONSE_RECEIVED
} from '../actions/demo';


export function graph(state = {
    data: {},
    service: ''
}, action) {
    switch (action.type) {
        case GRAPH_DATA_RECEIVE:
            return Object.assign({}, state, {
                data: action.data,
                service: action.service
            });
        case RESPONSE_RECEIVED:
            return Object.assign({}, state, {
                data: action.data,
                service: action.service
            });
        default:
            return state;
    }
}