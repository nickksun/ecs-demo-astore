import React, {Component} from 'react';


export default class Footer extends Component {

    render() {
        // const {dispatch} = this.props;
        return (
            <div className="footer_bg">
                <div className="wrap">
                    <div className="footer">

                        <div className="grids_of_4">
                            <div className="grid1_of_4">
                                <h4>featured sale</h4>
                                <ul className="f_nav">
                                    <li><a href="">alexis Hudson</a></li>
                                    <li><a href="">american apparel</a></li>
                                    <li><a href="">ben sherman</a></li>
                                    <li><a href="">big buddha</a></li>
                                    <li><a href="">channel</a></li>
                                    <li><a href="">christian audigier</a></li>
                                    <li><a href="">coach</a></li>
                                    <li><a href="">cole haan</a></li>
                                </ul>
                            </div>
                            <div className="grid1_of_4">
                                <h4>mens store</h4>
                                <ul className="f_nav">
                                    <li><a href="">alexis Hudson</a></li>
                                    <li><a href="">american apparel</a></li>
                                    <li><a href="">ben sherman</a></li>
                                    <li><a href="">big buddha</a></li>
                                    <li><a href="">channel</a></li>
                                    <li><a href="">christian audigier</a></li>
                                    <li><a href="">coach</a></li>
                                    <li><a href="">cole haan</a></li>
                                </ul>
                            </div>
                            <div className="grid1_of_4">
                                <h4>women store</h4>
                                <ul className="f_nav">
                                    <li><a href="">alexis Hudson</a></li>
                                    <li><a href="">american apparel</a></li>
                                    <li><a href="">ben sherman</a></li>
                                    <li><a href="">big buddha</a></li>
                                    <li><a href="">channel</a></li>
                                    <li><a href="">christian audigier</a></li>
                                    <li><a href="">coach</a></li>
                                    <li><a href="">cole haan</a></li>
                                </ul>
                            </div>
                            <div className="grid1_of_4">
                                <h4>quick links</h4>
                                <ul className="f_nav">
                                    <li><a href="">alexis Hudson</a></li>
                                    <li><a href="">american apparel</a></li>
                                    <li><a href="">ben sherman</a></li>
                                    <li><a href="">big buddha</a></li>
                                    <li><a href="">channel</a></li>
                                    <li><a href="">christian audigier</a></li>
                                    <li><a href="">coach</a></li>
                                    <li><a href="">cole haan</a></li>
                                </ul>
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}