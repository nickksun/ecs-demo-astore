import React, {Component} from 'react';
import ReactKonva from 'react-konva';
import Konva from 'konva';

const {Layer, Rect, Stage, Image} = ReactKonva;

import {getGraphData, sendRequest} from '../actions/demo';

class ExtinguisherImage extends React.Component {

}

class FireImage extends React.Component {
    constructor(args) {
        super(args);
        this.interval = null;
        this.isLarge = false;
        this.width = 50;
        this.height = 50;
        this.state = {
            image: null
        };
        // this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        const {dispatch} = this.props;

        this.interval = setInterval(() => {
            dispatch(sendRequest('fire'));
        }, 5000);

        const image = new window.Image();
        image.src = 'assets/img/fire-1.gif';
        image.onload = () => {
            this.setState({
                image: image
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        const {data, service} = nextProps;
        if (service === 'fire' && data.large_fire_percentage) {
            if (!this.isLarge && Math.floor(Math.random() * 100) <= data.large_fire_percentage) {
                this.width *= 2;
                this.height *= 2;
                this.isLarge = true;
            } else if (this.isLarge === true) {
                this.width /= 2;
                this.height /= 2;
                this.isLarge = false;
            }
        }

        if (service === 'extinguisher') {
            if(data && data.result > 0) {

            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        const {x, y, onClick} = this.props;
        return (
            <Image
                image={this.state.image}
                onClick={onClick}
                x={x}
                y={y}
                width={this.width}
                height={this.height}
            />
        );
    }
}

export default class Demo extends Component {
    constructor(args) {
        super(args);
        this.interval = null;
        this.fires = [];
        this.extinguishers = [];
        this.clicks = 0;
        this.handleClick = this.handleClick.bind(this);
    }

    componentWillMount() {
        const {dispatch} = this.props;

        this.interval = setInterval(() => {
            dispatch(getGraphData('fire'));
        }, 5000);
    }

    handleClick() {
        const {dispatch} = this.props;
        this.clicks++;
        dispatch(sendRequest('extinguisher'));
    }

    componentWillReceiveProps(nextProps) {
        const {data, service} = nextProps;
        if (data && data.result > 0) {
            if (service === 'fire') {
                if (this.fires.length <= 0) {
                    for (var i = 0; i < data.result; i++) {
                        this.fires.push([(i + 1) * 50, (i + 1) * 50]);
                    }
                } else {
                    let delta = this.fires.length - data.result;
                    if (delta > 0) {
                        // for (var i = 0; i < delta; i++) {
                        //     this.fires.pop();
                        // }
                    } else {
                        let currentLength = this.fires.length;
                        for (var i = 0; i < delta * -1; i++) {
                            this.fires.push([(currentLength + i + 1) * 50, (currentLength + i + 1) * 50]);
                        }
                    }
                }
            }

            if (service === 'extinguisher') {
                var removeFireIndex = Math.floor(Math.random() * 100) % this.fires.length;
                this.extinguishers.push(0);

            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        // const {dispatch} = this.props;
        return (
            <div>
                <h3>Clicks: {this.clicks}</h3>
                <Stage width={500} height={500}>
                    <Layer>
                        {this.fires.map((rect, i) => {
                            return (
                                <FireImage x={rect[0]} y={rect[1]} index={i} {...this.props} onClick={this.handleClick}/>
                            )
                        })}

                    </Layer>
                </Stage>
            </div>
        )
    }
}