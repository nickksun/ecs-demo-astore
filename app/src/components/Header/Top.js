import React, {Component} from 'react';


export default class Top extends Component {

    render() {
        // const {dispatch} = this.props;
        return (
            <div className="header_bg">
                <div className="wrap">
                    <div className="header">
                        <div className="logo">
                            <a href="index.html"><img src="assets/astore/images/logo.png" alt=""/> </a>
                        </div>
                        <div className="h_icon">
                            <ul className="icon1 sub-icon1">
                                <li><a className="active-icon c1" href="#"><i>$300</i></a>
                                    <ul className="sub-icon1 list">
                                        <li><h3>shopping cart empty</h3><a href=""></a></li>
                                        <li><p>if items in your wishlit are missing, <a href="contact.html">contact
                                            us</a> to view them</p></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div className="h_search">
                            <form>
                                <input type="text" value=""/>
                                <input type="submit" value=""/>
                            </form>
                        </div>
                        <div className="clear"></div>
                    </div>
                </div>
            </div>
        )
    }
}