import React, {Component} from 'react';


export default class Menu extends Component {




    render() {
        const {dispatch, onCategoryClick} = this.props;
        return (

            <div className="header_btm">
                <div className="wrap">
                    <div className="header_sub">
                        <div className="h_menu">
                            <ul>
                                <li className="active"><a href="index.html">Home</a></li>
                                |
                                <li><a href="sale.html">sale</a></li>
                                |
                                <li><a href="#" onClick={(event) => onCategoryClick(1)}>hoodi</a></li>
                                |
                                <li><a href="accessories.html">accessories</a></li>
                                |
                                <li><a href="wallets.html">wallets</a></li>
                                |
                                <li><a href="sale.html">sale</a></li>
                                |
                                <li><a href="index.html">mens store</a></li>
                                |
                                <li><a href="shoes.html">shoes</a></li>
                                |
                                <li><a href="sale.html">vintage</a></li>
                                |
                                <li><a href="service.html">services</a></li>
                                |
                                <li><a href="contact.html">Contact us</a></li>
                            </ul>
                        </div>
                        <div className="top-nav">
                            <nav className="nav">
                                <a href="#" id="w3-menu-trigger"> </a>
                                <ul className="nav-list">
                                    <li className="nav-item"><a className="active" href="index.html">Home</a></li>
                                    <li className="nav-item"><a href="sale.html">Sale</a></li>
                                    <li className="nav-item"><a href="handbags.html">Handbags</a></li>
                                    <li className="nav-item"><a href="accessories.html">Accessories</a></li>
                                    <li className="nav-item"><a href="shoes.html">Shoes</a></li>
                                    <li className="nav-item"><a href="service.html">Services</a></li>
                                    <li className="nav-item"><a href="contact.html">Contact</a></li>
                                </ul>
                            </nav>
                            <div className="search_box">
                                <form>
                                    <input type="text" value="Search" onFocus="this.value = '';"
                                           onBlur="if (this.value == '') {this.value = 'Search';}"/>
                                    <input type="submit"
                                           value=""/>
                                </form>
                            </div>
                            <div className="clear"></div>

                        </div>
                        <div className="clear"></div>
                    </div>
                </div>
            </div>
        )
    }
}