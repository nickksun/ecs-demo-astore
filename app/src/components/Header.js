import React, {Component} from 'react';
import Top from '../components/Header/Top';
import Menu from '../components/Header/Menu';
import Slider from '../components/Header/Slider';

export default class Header extends Component {

    render() {
        // const {dispatch} = this.props;
        return (
            <div>
                <Top/>
                <Menu/>
                <Slider/>
            </div>

        )
    }
}