#!/bin/bash

extraArgs=""
BUCKET_NAME="nickksun-ecs-demo-logs"
DATE=$(date +%Y-%m-%d)
REGION="ap-southeast-2"
SOURCE_FOLDER="/var/log/ecs"
aws s3 sync $SOURCE_FOLDER s3://$BUCKET_NAME/$DATE
