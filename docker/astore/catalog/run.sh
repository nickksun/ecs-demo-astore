#!/bin/bash

args="$@"

args="$@ -p 80 --no-cors"
extraArgs=""
PARAMETER_STORE_NAME="catalog-some-endpoint"
REGION="ap-southeast-2"
SECRET_FIRE=$(aws ssm get-parameters --region $REGION --name $PARAMETER_STORE_NAME --with-decryption --output text | awk '{print $4,$5,$6,$7,$8}')

file=/data/db.json
if [ -f $file ]; then
    echo "Found db.json, trying to open"
    extraArgs="db.json"    
    # if [ "$SECRET_FIRE" != "" ]; then echo $SECRET_FIRE > $file; fi
fi

file=/data/file.js
if [ -f $file ]; then
    echo "Found file.js seed file, trying to open"
    extraArgs="file.js"
fi

if [ -z "$extraArgs" ]; then
	if [ -n "$API_ENDPOINT" ]; then
		extraArgs="$API_ENDPOINT"
	fi
fi

file=/data/routes.json
if [ -f $file ]; then
    echo "Found routes.json, trying to open"
    extraArgs="$extraArgs --routes routes.json"
fi

args="$args $extraArgs"
json-server $args
# json-server
